import Device from "../models/Device";
import Repair from "../models/Repair";
import RepairStatus from "../models/RepairStatus";
import RepairStatusLog from "../models/RepairStatusLog";
import User from "../models/User";

const resolvers = {
  Query: {
    repair: (parent, args) => Repair.findByPk(args.id),
    repairs: () => Repair.findAll(),
    device: (parent, args) => Device.findByPk(args.id),
    devices: () => Device.findAll(),
    repairStatuses: () => RepairStatus.findAll(),
    repairStatus: (parent, args) => RepairStatus.findByPk(args.id),
    repairStatusLogs: () => RepairStatusLog.findAll(),
    repairStatusLogByRepair: (parent, args) => RepairStatusLog.findAll({ where: { repair_id: args.repair_id } }),
    repairStatusLog: (parent, args) => RepairStatusLog.findByPk(args.id),
    users: () => User.findAll(),
    user: (parent, args) => User.findByPk(args.id)
  },
  Mutation: {
    addDevice: (parent, args) => Device.create(args),
    updateDevice: (parent, args) => Device.update(args, { where: { id: args.id } }),
    deleteDevice: (parent, args) => Device.destroy({ where: { id: args.id } }),
    addRepair: (parent, args) => Repair.create(args),
    updateRepair: (parent, args) => Repair.update(args, { where: { id: args.id } }),
    deleteRepair: (parent, args) => Repair.destroy({ where: { id: args.id } }),
    addRepairStatus: (parent, args) => RepairStatus.create(args),
    updateRepairStatus: (parent, args) => RepairStatus.update(args, { where: { id: args.id } }),
    deleteRepairStatus: (parent, args) => RepairStatus.destroy({ where: { id: args.id } }),
    addRepairStatusLog: (parent, args) => RepairStatusLog.create(args),
    updateRepairStatusLog: (parent, args) => RepairStatusLog.update(args, { where: { id: args.id } }),
    deleteRepairStatusLog: (parent, args) => RepairStatusLog.destroy({ where: { id: args.id } }),
    addUser: (parent, args) => User.create(args),
    updateUser: (parent, args) => User.update(args, { where: { id: args.id } }),
    deleteUser: (parent, args) => User.destroy({ where: { id: args.id } })
  }
}

export default resolvers;