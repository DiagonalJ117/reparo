const typeDefs = `#graphql

  type Repair {
    id: ID!,
    description: String!,
    device: Device!,
    device_state: String!,
    repair_status_logs: [RepairStatusLog]!
  }

  type RepairStatus {
    id: ID!,
    title: String!,
    stage_number: Int!,
    description: String
  }

  type RepairStatusLog {
    id: ID!,
    repair: Repair!,
    repair_status: RepairStatus!,
    description: String!
  }

  type Device {
    id: ID!,
    brand: String!,
    model: String!,
    device_type: String!,
    serial_number: String!
  }

  type User {
    id: ID!,
    username: String!,
    password: String!
  }

  type Query {
    repairs: [Repair]!,
    repair(id: ID!): Repair,
    repairStatuses: [RepairStatus]!,
    repairStatus(id: ID!): RepairStatus,
    repairStatusLogs: [RepairStatusLog]!,
    repairStatusLog(id: ID!): RepairStatusLog,
    repairStatusLogByRepair(repair_id: ID!): [RepairStatusLog]!,
    devices: [Device]!,
    device(id: ID!): Device,
    users: [User]!,
    user(id: ID!): User
  }
  type Mutation {
    addRepair(description: String!, device_id: ID!, device_state: String!): Repair!,
    updateRepair(id: ID!, description: String, device_id: ID!, device_state: String): Repair!,
    deleteRepair(id: ID!): Boolean!,
    addDevice(brand: String!, model: String!, device_type: String!, serial_number: String!): Device!,
    updateDevice(id: ID!, brand: String, model: String, device_type: String, serial_number: String): Device!,
    deleteDevice(id: ID!): Boolean!,
    addRepairStatusLog(repair_id: ID!, repair_status_id: ID!, description: String): RepairStatusLog!,
    updateRepairStatusLog(id: ID!, repair_id: ID!, repair_status_id: ID!, description: String): RepairStatusLog!,
    deleteRepairStatusLog(id: ID!): Boolean!,
    addRepairStatus(title: String!, stage_number: Int!, description: String): RepairStatus!,
    updateRepairStatus(id: ID!, title: String, stage_number: Int, description: String): RepairStatus!,
    deleteRepairStatus(id: ID!): Boolean!,
    addUser(username: String!, password: String!): User!,
    updateUser(id: ID!, username: String, password: String): User!,
    deleteUser(id: ID!): Boolean!
  }
`;

export default typeDefs;