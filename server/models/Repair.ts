import { DataTypes } from "sequelize";
import sequelize from "../config/database";

const Repair = sequelize.define('Repair', {
  description: {
    type: DataTypes.STRING,
    allowNull: true
  },
  device_id: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  device_state: {
    type: DataTypes.TEXT,
    allowNull: false
  },
});

export default Repair;