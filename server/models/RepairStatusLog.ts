import { DataTypes } from "sequelize";
import sequelize from "../config/database";

const RepairStatusLog = sequelize.define('RepairStatusLog', {
  repair_id: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  repairstatus_id: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  description: {
    type: DataTypes.TEXT,
    allowNull: true
  }
});

export default RepairStatusLog;