import { DataTypes } from "sequelize";
import sequelize from "../config/database";

const RepairStatus = sequelize.define('RepairStatus', {
  title: {
    type: DataTypes.STRING,
    allowNull: false
  },
  stage_number: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  description: {
    type: DataTypes.TEXT,
    allowNull: true
  }
});

export default RepairStatus;
