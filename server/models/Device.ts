import { DataTypes } from "sequelize";
import sequelize from "../config/database";

const Device = sequelize.define('Device', {
  brand: {
    type: DataTypes.STRING,
    allowNull: false
  },
  model: {
    type: DataTypes.STRING,
    allowNull: false
  },
  device_type: {
    type: DataTypes.STRING,
    allowNull: false
  },
  serial_number: {
    type: DataTypes.STRING,
    allowNull: false
  },
});

export default Device;