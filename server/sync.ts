import sequelize from "./config/database";

sequelize.sync({ force: true }).then(() => {
  console.log(`Database & tables created!`);
})
.catch((err) => {
  console.error('Unable to sync the database', err);
});